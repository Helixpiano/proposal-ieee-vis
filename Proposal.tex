% $Id: template.tex 11 2007-04-03 22:25:53Z jpeltier $

\documentclass{vgtc}                          % final (conference style)
%\documentclass[review]{vgtc}                 % review
%\documentclass[widereview]{vgtc}             % wide-spaced review
%\documentclass[preprint]{vgtc}               % preprint
%\documentclass[electronic]{vgtc}             % electronic version

%% Uncomment one of the lines above depending on where your paper is
%% in the conference process. ``review'' and ``widereview'' are for review
%% submission, ``preprint'' is for pre-publication, and the final version
%% doesn't use a specific qualifier. Further, ``electronic'' includes
%% hyperreferences for more convenient online viewing.

%% Please use one of the ``review'' options in combination with the
%% assigned online id (see below) ONLY if your paper uses a double blind
%% review process. Some conferences, like IEEE Vis and InfoVis, have NOT
%% in the past.

%% Figures should be in CMYK or Grey scale format, otherwise, colour 
%% shifting may occur during the printing process.

%% These few lines make a distinction between latex and pdflatex calls and they
%% bring in essential packages for graphics and font handling.
%% Note that due to the \DeclareGraphicsExtensions{} call it is no longer necessary
%% to provide the the path and extension of a graphics file:
%% \includegraphics{diamondrule} is completely sufficient.
%%
\ifpdf%                                % if we use pdflatex
  \pdfoutput=1\relax                   % create PDFs from pdfLaTeX
  \pdfcompresslevel=9                  % PDF Compression
  \pdfoptionpdfminorversion=7          % create PDF 1.7
  \ExecuteOptions{pdftex}
  \usepackage{graphicx}                % allow us to embed graphics files
  \DeclareGraphicsExtensions{.pdf,.png,.jpg,.jpeg} % for pdflatex we expect .pdf, .png, or .jpg files
\else%                                 % else we use pure latex
  \ExecuteOptions{dvips}
  \usepackage{graphicx}                % allow us to embed graphics files
  \DeclareGraphicsExtensions{.eps}     % for pure latex we expect eps files
\fi%
\usepackage{subfig}
%% it is recomended to use ``\autoref{sec:bla}'' instead of ``Fig.~\ref{sec:bla}''
\graphicspath{{figures/}{pictures/}{images/}{./}} % where to search for the images

\usepackage{microtype}                 % use micro-typography (slightly more compact, better to read)
\PassOptionsToPackage{warn}{textcomp}  % to address font issues with \textrightarrow
\usepackage{textcomp}                  % use better special symbols
\usepackage{mathptmx}                  % use matching math font
\usepackage{times}                     % we use Times as the main font
\renewcommand*\ttdefault{txtt}         % a nicer typewriter font
\usepackage{cite}                      % needed to automatically sort the references
\usepackage{tabu}                      % only used for the table example
\usepackage{booktabs}                  % only used for the table example
%% We encourage the use of mathptmx for consistent usage of times font
%% throughout the proceedings. However, if you encounter conflicts
%% with other math-related packages, you may want to disable it.

% !TeX spellcheck = en_GB 
%% If you are submitting a paper to a conference for review with a double
%% blind reviewing process, please replace the value ``0'' below with your
%% OnlineID. Otherwise, you may safely leave it at ``0''.
\onlineid{0}

%% declare the category of your paper, only shown in review mode
\vgtccategory{Research}

%% allow for this line if you want the electronic option to work properly
\vgtcinsertpkg

%% In preprint mode you may define your own headline.
%\preprinttext{To appear in an IEEE VGTC sponsored conference.}

%% Paper title.

\title{Analysis and visualisation of additive manufacturing data for process monitoring}

%% This is how authors are specified in the conference style

%% Author and Affiliation (single author).
%%\author{Roy G. Biv\thanks{e-mail: roy.g.biv@aol.com}}
%%\affiliation{\scriptsize Allied Widgets Research}

%% Author and Affiliation (multiple authors with single affiliations).
%%\author{Roy G. Biv\thanks{e-mail: roy.g.biv@aol.com} %
%%\and Ed Grimley\thanks{e-mail:ed.grimley@aol.com} %
%%\and Martha Stewart\thanks{e-mail:martha.stewart@marthastewart.com}}
%%\affiliation{\scriptsize Martha Stewart Enterprises \\ Microsoft Research}

%% Author and Affiliation (multiple authors with multiple affiliations)
\author{Philipp Heinrich\thanks{e-mail: heinrich@zib.de}\\ %
        \scriptsize Zuse Institute Berlin %
}


\begin{document}

%% The ``\maketitle'' command must be the first command after the
%% ``\begin{document}'' command. It prepares and prints the title block.

%% the only exception to this rule is the \firstsection command
\firstsection{Introduction}

\maketitle

%% \section{Introduction} %for journal use above \firstsection{..} instead
Additive manufacturing (AM) is an upcoming technology that allows the construction of objects with complex structures and forms out of one single unit. It provides a better accuracy for complicated specimens compared to traditional manufacturing methods and opens up the possibility of rapid prototyping. One of the most common techniques used in additive manufacturing is selective laser melting (SLM) which is one of the powder bed fusion methods. In this process, a selected part of a layer of metal powder, which corresponds to a slice of the part to be manufactured, is molten by a laser inside a chamber. Afterwards, the building platform gets lowered and is recoated again with powder by a roller (see \autoref{fig:SLM}). The thickness of a single layer usually ranges from $\approx$ $20-60 \mu m$, dependant on the grain size of the used powder. Doing this procedure for consecutive layers creates the desired object. An entire building process might include building hundreds or even thousands of layers with manufacturing time ranging from minutes to many hours \cite{Gobert:2018}.

High-quality control of the built objects is important since they get used for example in aerospace and medical applications where failing parts can be a serious thread. Furthermore, it is necessary to ensure consistent results in the build process to allow serial production. It is fundamental to avoid large downtime of the machine for inspection of the manufactured specimen in order to provide an effective production. There is a plethora of possible defects: lack of fusion, porosity, form errors, delamination of layers and many more. They are caused by an equally large number of reasons including: too high or too low laser energy, a wrong movement speed of the laser, and distortions in the powder bed. Because of this, it is important to develop a consistent monitoring system to improve part quality control.

The currently predominantly taken way of quality assurance is post procession. Most of the time, X-ray micro-computed tomography ($\mu$CT) is used as a post-process quality control tool which falls short for defects that do not change the density of the object. A major drawback of post-process investigation is that the part might get rejected after the entire build process finished. Especially the lack of real time non-destructive inspections of specimens is currently holding back the industrial breakthrough of additive manufacturing. Real-time online monitoring would allow one to adjust parameters during the build process, repair in-situ or cancel it entirely to save time and resources. It is not sufficient to only monitor the building parameters of the machine, because errors can occur at random during the process e.g.\ due to powder clumping.
One of the most often chosen approaches for online monitoring is observing the melt pool which requires expensive high-speed cameras and complicated analysis due to the emissivity of the melt pool. Therefore, recent attempts have been using layer-wise process monitoring methods like optical tomography and inspections of the powder bed.

For this project, thermography and optical tomography data are being investigated for their suitability for online monitoring. Additionally, $\mu$CT data is being used to create a ground truth of defects.
The goal of this thesis is to create an interactive explorative tool which allows the user to gather a better understanding of the used methods and physics during the build process. A focus will be put on the question whether the used online monitoring methods are able to detect the same defects as $\mu$CT and whether this happens in the layer of the defect itself or only in subsequent layers. Another task is going to be the analysis and interpretation of the results. Furthermore, we hope to get a better grasp on which method is the most promising one to enable online monitoring in the future.
Challenges arise because contrary to many other cases the data sets are not complementing each other but rather partially overlap which makes the visualization more taxing since there is no clear correlation between image values for different methods. In addition displaying many small structures instead of a modicum of large ones requires new ideas to avoid a loss of clarity. Complexity is added due to thermography and optical tomography not representing the actual geometry of the specimen but only record effects on the specimen during the build process. 
\begin{figure}[tb]
	\centering
	\includegraphics[width=\columnwidth]{slm_illustration.JPG}
	\caption{Schema of Selective Laser Melting process \cite{Spears:2016}. A laser melts selected parts of a powder layer. The build plate gets lowered once a melt cycle is completed. Afterwards, the piston with the powder reservoir goes up so that the recoater can distribute it while spare grains drops down into a separate container.}
	\label{fig:SLM}
\end{figure}

\section{Related Work}
The first three subsections address the imaging methods that were used to create the data being analysed in this project. This is followed by a subsection regarding visualization.

\subsection{Thermography}
Laser processes like selective laser melting are mostly thermal processes, being therefore subject to investigation by infrared monitoring. 
Thermography is used both in situ and post-fabrication with cameras usually operating in the short-wavelength infrared or mid-wavelength infrared ($1.5\mu m - 5\mu m$) \cite{Hassler:2016}. They calculate the temperature of the monitored object based on the measured intensity of the radiation and Planck's law.
Defects become visible for the thermal cameras because they change the cooling rate and the temperature gradient due to distortions in the heat flow or heat capacity. It is important to point out that these characteristics depend on many process parameters like the scan velocity, scan line length, hatch distance (separation distance between adjacent scan tracks) and geometrical layout of the part being built. A consequence of basically all laser patterns in selective laser melting is the remelting of areas due to the laser lines being close together. This complicates the cooling analysis because of variations in the cooling behaviour based on the position in the specimen \cite{Krauss:2012} .\\
Post process investigation methods reheat the specimen and try to detect defects with an eventual reconstruction of their geometry.
In situ monitoring is separated into \textit{part monitoring} and \textit{melt pool monitoring}. 
Melt pool monitoring observes the total area and width-to-length ratio of the melt pool. Cameras are therefore required to have a high frame rate of around 10 kHz.
Part monitoring refers to observing an entire layer during the build process with a camera by either taking long exposure images or many single images. Given a high spatial and temporal resolution, it is even possible to observe particles that rapidly get ejected from the melt pool and cause noise in the images.
One major drawback of thermography is the high price of the cameras.

\subsection{Optical Tomography}
Optical tomography is a cheaper version of the thermography that operates at different wavelengths. It is one of the latest monitoring methods and got introduced by Zenzinger et al. \cite{Zenzinger:2015} in 2015. It utilizes a camera system that observes an entire layer during the build process. This high-resolution camera either records a single long exposure image or several images which get stacked later on. Structural disorders or a structural irregularities are indicated by any variation in the detected radiation intensity. It is possible to record the radiation with a normal camera because parts of the emitted thermal radiation are detectable in the visible spectrum ($\approx 380-780 nm$). This is convenient because these kinds of cameras are cheaper than the ones that get used in thermography. Thermal radiation is the only emission of interest in this method since it is directly connected to the melt pool. 
Therefore a band-pass filter is used to remove the radiation caused by plasma ($\approx 400-600nm$) and reflected laser light.

\subsection{X-ray micro-computed tomography}
X-ray tomography (XCT) is one of the oldest and most widespread post fabrication methods for detecting defects in additive manufacturing. It generates a 3D image by taking several x-ray images of a specimen around an axis of rotation followed by algorithms that reconstruct the image. The intensity of the received voxels is based on the amount of absorbed x-ray intensity. At the beginning it got mostly used in combination with additive manufacturing for medical purposes with the oldest paper \cite{Mankovich:1990} dating back to 1990 where X-ray computed tomography got used to reverse engineer a model of a skull. The first time XCT was used for three dimensional measurements was in the work of Berry et al. \cite{Berry:1997}. X-ray micro-computed tomography ($\mu$CT) as a method for pore detection began its rise in the mid-2000s with the focus of recent papers shifting towards an investigation of the surface morphology (see \cite{Zanini:2019,Zekavat:2018}).\\
It is widely used because of its function as \textit{ground truth} due to the ability to detect many common defects like pores, keyholes, and lack of fusion. The only shortcomings of $\mu$CT are defects that do not change the density of the measured specimen, e.g.\ delamination, or the investigation of materials with an extremely high density. 

\subsection{Visualization}
Visualization has so far not been even a minor topic in additive manufacturing because nearly every paper focuses on the practicality and functionality of a single method which makes laborious visualization of multimodal data redundant. On the contrary visualization of multimodal data is frequently utilized in other scientific fields like for example Neurosurgery to observe tumours and to plan surgeries. The main difference is that their datasets are mostly complementary while the datasets in additive manufacturing are likely to show similar results which causes a lot of spatial overlap. Baum \cite{Baum:2008} showed a method of displaying multimodal breast image data in 2D by taking advantage of either colour overlay or notably colour mixing. Another common 2D technique to visualize two datasets is the blending of two images. 
While there are some works like those by Irimia et al. \cite{Irimia:2011} and Lindemann et al. \cite{Lindemann:2013} which use binary masks to visualize a brain tumour recorded with different methods in 3D, they do not broach the issue of overlay in their data. The problem of overlapping data in 3D space is often avoided by utilizing a cut-out which discards the part of the volume where the viewpoint is located. An alternative to this is, for example, the creation of a magical lens that allows skipping certain sections like skin and bones or in as our case the surface of the specimen.

\section{Data} \label{section:data}
A total of three datasets consisting of one post process and two in situ measurements were available for the analysis. This section gives a brief overview of the received data.
 
\subsection{Specimen}
The specimen itself was built with Steel 316L in an SLM 280 HL machine. It consists of a 10x10x5 mm cuboid as base built with standard parameters and contains several blank spaces as artificial defects. On top of the base are two 5x5x5 mm towers, one built with high and the other one with low energy parameter settings. Additionally, there are two ramps with inclinations of $40^\circ$ and $45^\circ$ \autoref{fig:Specimen}. A total of 200 layers were built with a thickness of $50\mu m$ each. Hatch distance was 0.12 mm, using an alternating stripe pattern with a $90^\circ$ rotation after each layer.

\begin{figure}[tb]
	\centering
	\includegraphics[width=\columnwidth]{Specimen.PNG}
	\caption{Digital image of the specimen and real photo in comparison.}
	\label{fig:Specimen}
\end{figure}

\subsection{Thermography} \label{subsec:Thermography}
This dataset was recorded with a Mid-Wavelength-Infrared camera. The resolution is 130x160 with the specimen itself occupying 102x102 Pixels and a spatial resolution of around $98\mu m$. The camera started to record at the beginning of every new layer at a frequency of 900 Hz. The recording stopped after 2500 frames with around 1100 frames showing the build process. The frame rate makes it look like the laser is jumping in-between frames because it moves too fast for the camera. A side effect of this is that the maximum of the temperature will just be measured for a couple of the pixels in every layer. In addition, the camera measures lots of noise due to ejected particles from the melt pool. Furthermore, the temperature in the tower which was built with high energy parameters exceeded the camera calibration and therefore delivered the same value at most points. The temperature values given by the camera are only ``apparent temperature'' because the provided algorithm is not capable of converting the measured intensity correctly into temperature. 

\subsection{Optical Tomography}
The optical tomography camera recorded the entire build chamber with a resolution of 3008x4112 pixels. The size of the specimen is approximately 210x210 pixels in the images with a spatial resolution of around $46\mu m$. A total of 10 out of 200 layers did not get recorded due to the camera not getting triggered properly. All missing layers got replaced by their corresponding prior one.

\subsection{X-ray micro-computed tomography}
The specimen was scanned in a $\mu$CT machine after the build process was finished. The resulting data have a resolution of 2001x2001x1519. Around 1300x1300x1100 voxels display the specimen with a each one having a size of 7.1213x7.1213x7.1213 $\mu m$. The bottom of the base ($\approx 2 mm \sim 40$ layers) was cut off during the removal of the specimen from the build plate.

\section{Preliminary Methods}
The aim of this work is to develop a visual data analysis tool for the available data to enable an initial analysis and comparison of the investigated imaging methods with regard to their suitability for online monitoring. For this cause, we forged a 3D image which displays distinctive areas of the datasets that might be caused by defects.\\
The first step was to produce a transparent envelope of the specimen in order to provide sufficient guidance for a viewer during the investigation of the datasets. For this, the shape of the $\mu$CT scan was chosen because it is an actual representation of the geometry of the specimen.
Using watershed segmentation in the visualization software \textit{Amira} allowed the extraction of the $\mu$CT surface.
Furthermore, Amira offers modules which provide a comfortable way to extract the pores of the $\mu$CT data. At this juncture, a threshold was automatically computed based on the histogram of the $\mu$CT data with the resulting binary image containing all voxels below the calculated threshold. This image also contained the surrounding of the specimen because the environment has low measured values just like the defects. This surrounding area was removed by another module with the result being a label field containing only the defects of the $\mu$CT data.
 
The next step was to extract information about the defects from the thermography data since the maximum temperature is not a viable benchmark (as explained in \autoref{subsec:Thermography}). The chosen approach was to take the time a pixel spent over a temperature threshold based on the assumption that a defect causes a worse heat distribution which will result in slower cooling of the surrounding and therefore a longer time at high temperatures. In our case, we chose 650K as the threshold because it provides enough information while defects become distinguishable enough from the background. Another approach would have been the analysis of the cooling rate of the specimen, presuming that changes in the cooling rate point towards defects because of the changed heat distribution.\\
Afterwards, the optical tomography and thermography data got registered on the $\mu$CT data with the software \textit{elastix} \cite{Klein:2010}.
The chosen metric was advanced Mattes mutual information with adaptive stochastic gradient descent as an optimizer and 500 iteration steps at 4 different resolutions.

Lastly, Amiras volume rendering got used to display a transparent version of the specimen's surface, $\mu$CT pores and highest intensities in optical tomography and thermography (see for example \autoref{fig:Overlap_CT_thermography}). A careful choice of colours is therefore important to make different combinations of data easily readable and distinguishable.

\begin{figure}[tb]
	\centering
	\includegraphics[width=\columnwidth]{CT_thermography.png}
	\caption{Volume rendering of the overlap from $\mu$CT and thermography. Blue - $\mu$CT defects, - Red thermography defects.}
	\label{fig:Overlap_CT_thermography}
\end{figure}

\section{Preliminary Results}\label{p-results}
This is a summary of the preliminary results that have been achieved so far:
\begin{enumerate}

	\item Optical tomography and thermography showed their principle capability of detecting defects by measuring high intensities in areas which correspond shape wise to major defects in $\mu$C. It is important to point out that this does not imply that they show exactly the same results. A comparison of these results shows that the areas only partially overlap. Furthermore, optical tomography displays structures in a more detailed way while thermography displays the same structures in an extended manner (see \autoref{fig:Tomo_Thermo}). The resolution of the data is one of the reasons for this because the spatial resolution of the optical tomography camera is twice the spatial resolution of the thermography camera and therefore enables the detection of finer structures. Another reason for this phenomenon is that optical tomography observes the local radiation of the melt pool while thermography detects the effect on the heat distribution in the entire surrounding.
	
	\item Using elastix showed that a simple rigid or similarity transformation is not sufficient to register the data properly. Comparing the layer numbers in thermography and optical tomography in which defects got detected with the corresponding layer numbers in $\mu$CT data revealed that the real specimen got non-uniformly compressed during the build process. Therefore it is necessary to use at least an affine transformation to register the images properly (\autoref{fig:CT}). 
	
	\item Analysing the cooling rate of the specimen based on thermography data is not a viable strategy with the currently available datasets. Observing the development of a pixel in a layer showed that each point on the specimen gets reheated two to three times by the laser which prevents constant cooling (\autoref{fig:Temperature_profile}). Additionally, the time until the laser returns to reheat this point strongly varies depending on its position in the layer. This leads to very inconsistent results that are mostly based on the maximum temperature at this very pixel which is problematic because of the unreliability of the maximum temperature as explained in \autoref{section:data}. Lastly, it is necessary to point out that long term cooling analysis is not possible due to the calibration range of the camera and the material that gets ejected from the melt pool, therefore, causing noise during the recording.
\end{enumerate}

\begin{figure*}[h!]%
	\centering
	\subfloat[Comparison of a major defect as seen in layer 145 in thermography (left) and optical tomography (right).\label{fig:Tomo_Thermo}]{{\includegraphics[width=0.6\columnwidth]{Tomo_Thermo2.jpg} }}%
	\qquad
	\centering
	\subfloat[Volume rendering of $\mu$CT scan of the specimen showing a clearly visible dent on top of the right tower.	\label{fig:CT}]{{\includegraphics[width=0.6\columnwidth]{CT_opposite_cavity.png} }}%
	\qquad
	\subfloat[Temperature profile of a single point on the specimen recorded by the thermography camera.]{{\includegraphics[width=0.6\columnwidth]{Temperature_profile.pdf} }}%
	\caption{}
	\label{fig:Temperature_profile}%
\end{figure*}

\section{Proposed Work}
The first step to proceed with this work is the acquisition of more data from other methods. It is planned to take pictures of the powder bed, as proposed by Craeghs et al. \cite{Craeghs:2011}, to detect disturbances due to e.g.\ a deformity of the recoater which inevitably forces defects. One additional method is the so-called \textit{eddy current} which is currently still under development and will be available at a later point of the project.\\
A new short-wavelength infrared camera will be added to the selective laser melting machine in addition to the currently used mid-wavelength infrared camera. This camera will be dedicated to the observation of the long term cooling rate of the specimen which is at the moment not covered due to the calibration range of the mid-wavelength infrared camera (as mentioned under point 3 in \autoref{p-results}).

The concept of linking and brushing is most likely going to play a major role in the upcoming analysis. The goal is to offer a highly flexible system to highlight areas which meet certain conditions like extraordinary intensities and to analyse these regions in detail by offering specific tools for the analysis of the specific type of data. Ideas for displayed information are the percentage of overlap, the volume of the highlighted regions and intensity differences compared to intact regions.
A similar concept is the formation of a data analysis lens comparable to the one used by Kirmizibayrak et al. \cite{Kirmizibayrak:2012} which shows statistic for currently viewed or selected areas.
There will be a need to develop completely new tools to examine the available datasets because some inconveniences might occur during the search for overlapping areas. One of those is, that due to the fact that observed in-situ data is often only a side effect of the build process, it is not guaranteed that they appear in the same layer as the defect itself or if they appear in following layers. This behaviour might lead to some trouble when only explicitly looking for overlaying regions.

Of further importance is the visualization of overlapping areas. Besides a good choice of colour and changes in intensity, it is important to figure out how to avoid loss of information due to data-points obscuring areas of interest.
For this, it is mandatory to reduce fuzziness that might be seen in the data (as observed in the $\mu$CT data of \autoref{fig:Overlap_CT_thermography}) without leaving out important information to provide a better overview for the observer.
For single cases it might be interesting to take the approach of colour mixing \cite{Baum:2008} to create 2D images out of the multimodal data. This requires a careful selection of colour space with many possible choices like classical RGB, $L*a*b$ and CIE XYZ.

\section{Discussion}
While simulations are surely able to grant a better understanding of the build process itself they are very complex and it is unclear how much they will be able to help this project. For sure it is important to understand the development of defects but the available data is mostly based on radiation emitted by the melt pool which would create an additional layer of complexity in the simulation. Furthermore, as mentioned by King et al \cite{King:2015}, it would be necessary to determine whether the simulation should be on part size or rather on grain size with each system featuring countless challenges and idiosyncrasies.

One of the topics with rising interest in the visualization community is virtual reality. It might be worth to consider it at the very end of this project but it does not seem likely that virtual reality offers any advantage for answering scientific questions regarding additive manufacturing at the moment.

\section{Questions for the panel}
\begin{enumerate}
\item Are there any paper which examine visualization of overlapping multimodal data in 3D?
\item Ideas for tools that will support further analysis?
\end{enumerate}
\section{Conclusion}
Our preliminary results demonstrate the benefit of visualization for analysing defects in additive manufacturing. All in all, there are many new possibilities to be uncovered that improve quality assurance in selective laser melting. This project will provide a pivotal role in understanding the capabilities of different measurement methods in detail while making progress towards reliable online monitoring.

\acknowledgments{
Hier die Zusammenarbeit mit der BAM nennen?}

\bibliographystyle{abbrv-doi}
\bibliography{Proposal}
\end{document}
